// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import httpRequest from '@/utils/httpRequest' // api: https://github.com/axios/axios

import './assets/css/bootstrap.css'
import './assets/css/font-awesome.min.css'
import './assets/css/main.css'
import './assets/css/my-custom-styles.css'
import './assets/iconfont/iconfont.css'

import './assets/js/bootstrap.min.js'
import './assets/js/plugins/slick/slick.min.js'
import './assets/js/plugins/stellar/jquery.stellar.min.js'
import './assets/js/repute-scripts.js'

import '@/element-ui' // api: https://github.com/ElemeFE/element
import '@/element-ui/theme/element-#406DA4/index.css';

// 非生产环境, 适配mockjs模拟数据                 // api: https://github.com/nuysoft/Mock
if (process.env.NODE_ENV !== 'production') {
  require('@/mock')
}

Vue.config.productionTip = false

// 挂载全局
Vue.prototype.$http = httpRequest // ajax请求方法

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'
})
