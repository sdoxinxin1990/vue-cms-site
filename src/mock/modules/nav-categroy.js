/**
 *
 * 考虑：
 *
 * 1. 根据type判断栏目类型，0、父级栏目导航无动作，1、栏目导航，2、跳转外部链接并打开新页，3、跳转外部链接本页打开，4、动态路由不显示在顶部导航中
 */

var navDataList = [{
    'menuId': 1,
    'parentId': 0,
    'parentName': null,
    'name': '关于',
    'url': 'about',
    'params': null,
    'type': 1,
    'icon': null,
    'orderNum': 1,
    'open': null,
    'list': null
  },
  {
    'menuId': 2,
    'parentId': 0,
    'parentName': null,
    'name': '新闻',
    'url': null,
    'params': null,
    'type': 0,
    'icon': null,
    'orderNum': 2,
    'open': null,
    'list': [{
      'menuId': 20,
      'parentId': 2,
      'parentName': null,
      'name': '[新闻详细页]',
      'url': 'news/single',
      'params': {
        'categoryId': '',
        'newsId': ''
      },
      'type': 4,
      'icon': null,
      'orderNum': 1,
      'open': null,
      'list': null
    }, {
      'menuId': 21,
      'parentId': 2,
      'parentName': null,
      'name': '公司要闻',
      'url': 'news/list',
      'params': {
        'id': 21
      },
      'type': 1,
      'icon': null,
      'orderNum': 1,
      'open': null,
      'list': null
    }, ]
  },
  {
    'menuId': 3,
    'parentId': 0,
    'parentName': null,
    'name': '案例',
    'url': 'case/list',
    'params': {
      'id': 3
    },
    'type': 1,
    'icon': null,
    'orderNum': 3,
    'open': null,
    'list': null
  },
  {
    'menuId': 30,
    'parentId': 0,
    'parentName': null,
    'name': '[案例详细页]',
    'url': 'case/single',
    'params': {
      'categoryId': '',
      'caseId': ''
    },
    'type': 4,
    'icon': null,
    'orderNum': 1,
    'open': null,
    'list': null
  },
  {
    'menuId': 4,
    'parentId': 0,
    'parentName': null,
    'name': '服务',
    'url': 'service',
    'params': null,
    'type': 1,
    'icon': null,
    'orderNum': 4,
    'open': null,
    'list': null
  },
  {
    'menuId': 5,
    'parentId': 0,
    'parentName': null,
    'name': '联系',
    'url': 'contacts',
    'params': null,
    'type': 1,
    'icon': null,
    'orderNum': 5,
    'open': null,
    'list': null
  },
  {
    'menuId': 6,
    'parentId': 0,
    'parentName': null,
    'name': '文档',
    'url': 'http://localhost:8080/guide',
    'params': null,
    'type': 1,
    'icon': 'fa-book',
    'orderNum': 5,
    'open': null,
    'list': null
  }
]

// 获取导航菜单列表
export function nav() {
  return {
    isOpen: true,
    url: '/nav/category',
    type: 'get',
    data: {
      'msg': 'success',
      'code': 0,
      'categoryList': navDataList,
    }
  }
}
